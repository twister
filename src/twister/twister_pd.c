/*
  This file is part of Taler.
  Copyright (C) 2024 Taler Systems SA

  Taler is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3,
  or (at your option) any later version.

  Taler is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with Taler; see the file COPYING.  If not,
  write to the Free Software Foundation, Inc., 51 Franklin
  Street, Fifth Floor, Boston, MA 02110-1301, USA.  */

/**
 * @file twister_pd.c
 * @brief Twister project data
 * @author Christian Grothoff
 */
#include "platform.h"
#include <gnunet/gnunet_util_lib.h>
#include "taler_twister_service.h"


/**
 * Default project data used for installation path detection
 * for Twister.
 */
static const struct GNUNET_OS_ProjectData twister_project_data = {
  .libname = "libtalertwister",
  .project_dirname = "twister",
  .binary_name = "taler-twister",
  .version = PACKAGE_VERSION " " VCS_VERSION,
  .env_varname = "TWISTER_PREFIX",
  .base_config_varname = "TWISTER_BASE_CONFIG",
  .bug_email = "taler@gnu.org",
  .homepage = "http://www.gnu.org/s/taler/",
  .config_file = "twister.conf",
  .user_config_file = "~/.config/twister.conf",
  .is_gnu = 1,
  .gettext_domain = "twister",
  .gettext_path = NULL,
  .agpl_url = GNUNET_AGPL_URL,
};


const struct GNUNET_OS_ProjectData *
TWISTER_project_data ()
{
  return &twister_project_data;
}
