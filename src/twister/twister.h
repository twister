/*
     This file is part of GNUnet.
     Copyright (C) 2018 Taler Systems SA

     Twister is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version
     3, or (at your option) any later version.

     Twister is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
     the GNU General Public License for more details.

     You should have received a copy of the GNU General Public
     License along with Twister; see the file COPYING.  If not,
     write to the Free Software Foundation, Inc., 51 Franklin
     Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

/**
 * @author
 * @file twister.h
 *
 * @brief Common type definitions
 */
#ifndef TWISTER_H
#define TWISTER_H

#include <gnunet/gnunet_common.h>

#define TWISTER_MESSAGE_TYPE_ACKNOWLEDGEMENT 1

#define TWISTER_MESSAGE_TYPE_SET_RESPONSE_CODE 2

#define TWISTER_MESSAGE_TYPE_DELETE_PATH 3

#define TWISTER_MESSAGE_TYPE_MODIFY_PATH_DL 4

#define TWISTER_MESSAGE_TYPE_MODIFY_PATH_UL 5

#define TWISTER_MESSAGE_TYPE_MALFORM 6

#define TWISTER_MESSAGE_TYPE_MALFORM_UPLOAD 7

#define TWISTER_MESSAGE_TYPE_FLIP_PATH_DL 8

#define TWISTER_MESSAGE_TYPE_FLIP_PATH_UL 9

#define TWISTER_MESSAGE_TYPE_MODIFY_HEADER_DL 10

GNUNET_NETWORK_STRUCT_BEGIN
struct TWISTER_Malform
{
  /**
   * Type: #TWISTER_MESSAGE_TYPE_MALFORM
   */
  struct GNUNET_MessageHeader header;

};
GNUNET_NETWORK_STRUCT_END

GNUNET_NETWORK_STRUCT_BEGIN
struct TWISTER_ModifyPath
{
  /**
   * Type: #TWISTER_MESSAGE_TYPE_DELETE_PATH
   */
  struct GNUNET_MessageHeader header;
};
GNUNET_NETWORK_STRUCT_END


GNUNET_NETWORK_STRUCT_BEGIN

/**
 * Network size estimate sent from the service
 * to clients.  Contains the current size estimate
 * (or 0 if none has been calculated) and the
 * standard deviation of known estimates.
 *
 */
struct TWISTER_FlipPath
{
  /**
   * Type: #TWISTER_MESSAGE_TYPE_DELETE_PATH
   */
  struct GNUNET_MessageHeader header;
};

GNUNET_NETWORK_STRUCT_END



GNUNET_NETWORK_STRUCT_BEGIN

/**
 * Network size estimate sent from the service
 * to clients.  Contains the current size estimate
 * (or 0 if none has been calculated) and the
 * standard deviation of known estimates.
 *
 */
struct TWISTER_DeletePath
{
  /**
   * Type: #TWISTER_MESSAGE_TYPE_DELETE_PATH
   */
  struct GNUNET_MessageHeader header;
};
GNUNET_NETWORK_STRUCT_END


GNUNET_NETWORK_STRUCT_BEGIN

/**
 * Network size estimate sent from the service
 * to clients.  Contains the current size estimate
 * (or 0 if none has been calculated) and the
 * standard deviation of known estimates.
 *
 */
struct TWISTER_SetResponseCode
{
  /**
   * Type: #TWISTER_MESSAGE_TYPE_SET_RESPONSE_CODE
   */
  struct GNUNET_MessageHeader header;

  /**
   * The new response code, in big endian.
   */
  uint32_t response_code GNUNET_PACKED;

};
GNUNET_NETWORK_STRUCT_END


#endif
