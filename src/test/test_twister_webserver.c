/*
  This file is part of Taler.
  Copyright (C) 2009, 2010, 2011, 2016 GNUnet e.V.
  Copyright (C) 2018 Taler Systems SA

  Taler is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3,
  or (at your option) any later version.

  Taler is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with Taler; see the file COPYING.  If not,
  write to the Free Software Foundation, Inc., 51 Franklin
  Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

/**
 * This code is in the public domain.
 */
#include <limits.h>
#include <sys/types.h>
#ifndef _WIN32
#include <sys/select.h>
#include <sys/socket.h>
#else
#include <winsock2.h>
#endif
#include <string.h>
#include <microhttpd.h>
#include <stdio.h>

#define PORT 8080

static enum MHD_Result
answer_to_connection
  (void *cls, struct MHD_Connection *connection,
   const char *url, const char *method,
   const char *version, const char *upload_data,
   size_t *upload_data_size, void **con_cls)
{
#if 0
  const char *page = "<html><body>Hello, browser!</body></html>";
#endif
  const char *page = "{\"hello\": [{\"this\": \"browser!\"}],"\
                     "\"when\": \"today\"}";
  struct MHD_Response *response;
  int ret;

  (void)cls;               /* Unused. Silent compiler warning. */
  (void)url;               /* Unused. Silent compiler warning. */
  (void)method;            /* Unused. Silent compiler warning. */
  (void)version;           /* Unused. Silent compiler warning. */
  (void)upload_data;       /* Unused. Silent compiler warning. */

  if (NULL == (*con_cls))
    {
      *con_cls = "first";
      return MHD_YES;
    }
  if (0 != *upload_data_size)
    {
      *upload_data_size = 0;
      return MHD_YES;
    }
  response = MHD_create_response_from_buffer
    (strlen (page),
     (void *) page,
     MHD_RESPMEM_PERSISTENT);
  ret = MHD_queue_response (connection, MHD_HTTP_OK, response);
  MHD_destroy_response (response);

  return ret;
}

/**
 * This minimalist Web server listens on port PORT and responds
 * with a HTTP status 200 OK and some static "hello world" body,
 * _always_.
 */
int
main (void)
{
  struct MHD_Daemon *daemon;

  daemon = MHD_start_daemon (MHD_USE_AUTO
                             | MHD_USE_INTERNAL_POLLING_THREAD,
                             PORT, NULL, NULL,
                             &answer_to_connection, NULL,
                             MHD_OPTION_END);
  if (NULL == daemon)
    return 1;
  sleep (UINT_MAX);

  MHD_stop_daemon (daemon);
  return 0;
}
