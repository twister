#!/bin/sh

# This file is part of GNUnet.
# Copyright (C) 2018 Taler Systems SA
# 
# Twister is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version
# 3, or (at your option) any later version.
# 
# Twister is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
# the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public
# License along with Twister; see the file COPYING.  If not,
# write to the Free Software Foundation, Inc., 51 Franklin
# Street, Fifth Floor, Boston, MA 02110-1301, USA.

# @author Marcello Stanisci
# @author Christian Grothoff
# 
# @file test_twister.sh
# 
# @brief Twister testcases.


if ! which curl > /dev/null
then
    echo "curl not found";
    exit 77
fi

TWISTER_UNIXPATH="/tmp/http-twister.sock"
TWISTER_DUMMY_URL="http://dummyhost/"

# Launch the Web server.
./test_twister_webserver &
web_server_pid=$!

echo Webserver launched.

# Launch the Twister.
taler-twister-service -c ./test_twister_ipc.conf &
twister_service_pid=$!

echo Twister launched.

sleep 1
if ! ps xo pid | grep -q ${twister_service_pid}; then
  echo Twister did not start correctly
  return 77
fi

status_code=$(curl -s --unix-socket ${TWISTER_UNIXPATH} \
  ${TWISTER_DUMMY_URL} -o /dev/null -w "%{http_code}")

# check status code was hacked
if ! test 200 = $status_code; then
  echo "Could not reach the Twister (${status_code})"
  kill $web_server_pid
  kill $twister_service_pid
  exit 1
fi

echo "Twister reached via IPC".

# shutdown twister and webserver
kill $web_server_pid
kill $twister_service_pid

exit 0
