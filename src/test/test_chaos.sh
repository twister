#!/bin/sh

# This file is part of GNUnet.
# Copyright (C) 2018 Taler Systems SA
# 
# Twister is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version
# 3, or (at your option) any later version.
# 
# Twister is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
# the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public
# License along with Twister; see the file COPYING.  If not,
# write to the Free Software Foundation, Inc., 51 Franklin
# Street, Fifth Floor, Boston, MA 02110-1301, USA.

# @author Marcello Stanisci
# @author Christian Grothoff
# 
# @file test_twister.sh
# 
# @brief Twister testcases, with a 5% chaos probability (#5737).


if ! which curl > /dev/null
then
    echo "curl not found";
    exit 77
fi

TWISTER_URL_V4="http://localhost:8888/"
TWISTER_URL="http://[::1]:8888/"

# Launch the Web server.
./test_twister_webserver &
web_server_pid=$!

echo Webserver launched.

# Launch the Twister.
taler-twister-service -c ./test_chaos.conf &
twister_service_pid=$!

echo "Twister chaos'd launched (${twister_service_pid})."

# We give 100 rounds to match a 5% probability.
for i in $(seq 1 100); do
  status_code=$(curl -s ${TWISTER_URL_V4} -o /dev/null \
    -w "%{http_code}")
  
  # check status code was hacked
  if test 503 = $status_code; then
    echo "$i-th round: found 'Service Unavailable'; success."
    kill $web_server_pid
    kill $twister_service_pid
    exit 0
  fi
done

echo Could not find 'Service Unavailable response; fail!'
# shutdown twister and webserver
kill $web_server_pid
kill $twister_service_pid

exit 1
